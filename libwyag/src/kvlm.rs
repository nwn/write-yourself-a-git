pub fn parse(mut raw: &str) -> (Vec<(String, String)>, String) {
    let mut dict = vec![];
    let mut message = String::new();

    while !raw.is_empty() {
        // If the line is empty, it means the remainder of the data is the message.
        if raw.starts_with('\n') {
            message = raw[1..].to_owned();
            break;
        }

        // Search for the next space.
        if let Some(sep) = raw.find(' ') {
            let key = raw[..sep].to_owned();
            raw = &raw[sep+1..];

            let mut value = String::new();
            while let Some(sep) = raw.find('\n') {
                value.push_str(&raw[..sep]);
                raw = &raw[sep + 1..];
                if raw.starts_with(' ') {
                    value.push('\n');
                    raw = &raw[1..];
                } else {
                    break;
                }
            }

            // Insert the key/value into the map
            dict.push((key, value));
        }
    }

    (dict, message)
}

pub fn serialize(kvlm: &[(String, String)], message: &str) -> String {
	let mut result = String::new();

    // Output fields
    for (key, val) in kvlm {
        let val = val.replace('\n', "\n ");
        result.push_str(&format!("{} {}\n", key, val));
    }

    // Append message
	result.push('\n');
	result.push_str(message);

    result
}

#[cfg(test)]
mod tests {
    const COMMIT: &'static str = "\
tree 29ff16c9c14e2652b22f8b78bb08a5a07930c147
parent 206941306e8a8af65b66eaaaea388a7ae24d49a0
parent 66eaaaea388a7ae24d49a0206941306e8a8af65b
author Thibault Polge <thibault@thb.lt> 1527025023 +0200
committer Thibault Polge <thibault@thb.lt> 1527025044 +0200
gpgsig -----BEGIN PGP SIGNATURE-----
 
 iQIzBAABCAAdFiEExwXquOM8bWb4Q2zVGxM2FxoLkGQFAlsEjZQACgkQGxM2FxoL
 kGQdcBAAqPP+ln4nGDd2gETXjvOpOxLzIMEw4A9gU6CzWzm+oB8mEIKyaH0UFIPh
 rNUZ1j7/ZGFNeBDtT55LPdPIQw4KKlcf6kC8MPWP3qSu3xHqx12C5zyai2duFZUU
 wqOt9iCFCscFQYqKs3xsHI+ncQb+PGjVZA8+jPw7nrPIkeSXQV2aZb1E68wa2YIL
 3eYgTUKz34cB6tAq9YwHnZpyPx8UJCZGkshpJmgtZ3mCbtQaO17LoihnqPn4UOMr
 V75R/7FjSuPLS8NaZF4wfi52btXMSxO/u7GuoJkzJscP3p4qtwe6Rl9dc1XC8P7k
 NIbGZ5Yg5cEPcfmhgXFOhQZkD0yxcJqBUcoFpnp2vu5XJl2E5I/quIyVxUXi6O6c
 /obspcvace4wy8uO0bdVhc4nJ+Rla4InVSJaUaBeiHTW8kReSFYyMmDCzLjGIu1q
 doU61OM3Zv1ptsLu3gUE6GU27iWYj2RWN3e3HE4Sbd89IFwLXNdSuM0ifDLZk7AQ
 WBhRhipCCgZhkj9g2NEk7jRVslti1NdN5zoQLaJNqSwO1MtxTmJ15Ksk3QP6kfLB
 Q52UWybBzpaP9HEd4XnR+HuQ4k2K0ns2KgNImsNvIyFwbpMUyUWLMPimaV1DWUXo
 5SBjDB/V/W2JBFR+XKHFJeFwYhj7DD/ocsGr4ZMx/lgc8rjIBkI=
 =lgTX
 -----END PGP SIGNATURE-----

Create first draft
";

    #[test]
    pub fn parse_serialize() {
        let (fields, message) = super::parse(COMMIT);
        let commit = super::serialize(&fields, &message);
        assert_eq!(COMMIT, commit);
    }
}
