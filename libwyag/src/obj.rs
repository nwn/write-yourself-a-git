use super::repo::{self, Repository};
use std::any::Any;
use libflate::zlib::{Encoder, Decoder};

pub trait Object {
    fn fmt(&self) -> &'static str;
    fn serialize(&self) -> String;
    fn deserialize(&mut self, data: &str);
    fn as_any(&self) -> &dyn Any;
}

pub struct Commit {
    pub kvlm: (Vec<(String, String)>, String),
}
impl Commit {
    fn from_data(data: &str) -> Self {
        Self {
            kvlm: super::kvlm::parse(data),
        }
    }
}
impl Object for Commit {
    fn fmt(&self) -> &'static str {
        "commit"
    }
    fn serialize(&self) -> String {
        super::kvlm::serialize(&self.kvlm.0, &self.kvlm.1)
    }
    fn deserialize(&mut self, data: &str) {
        self.kvlm = super::kvlm::parse(data);
    }
    fn as_any(&self) -> &dyn Any {
        self
    }
}

pub struct Tag {
    pub kvlm: (Vec<(String, String)>, String),
}
impl Tag {
    fn from_data(data: &str) -> Self {
        Self {
            kvlm: super::kvlm::parse(data),
        }
    }
}
impl Object for Tag {
    fn fmt(&self) -> &'static str {
        "tag"
    }
    fn serialize(&self) -> String {
        super::kvlm::serialize(&self.kvlm.0, &self.kvlm.1)
    }
    fn deserialize(&mut self, data: &str) {
        self.kvlm = super::kvlm::parse(data);
    }
    fn as_any(&self) -> &dyn Any {
        self
    }
}

pub struct Tree {
    pub leaves: Vec<super::tree::Leaf>,
}
impl Tree {
    fn from_data(data: &str) -> Self {
        Self {
            leaves: super::tree::parse(data),
        }
    }
}
impl Object for Tree {
    fn fmt(&self) -> &'static str {
        "tree"
    }
    fn serialize(&self) -> String {
        super::tree::serialize(&self.leaves)
    }
    fn deserialize(&mut self, data: &str) {
        self.leaves = super::tree::parse(data);
    }
    fn as_any(&self) -> &dyn Any {
        self
    }
}

pub struct Blob {
    pub data: String,
}
impl Blob {
    fn from_data(data: &str) -> Self {
        Self { data: data.to_owned() }
    }
}
impl Object for Blob {
    fn fmt(&self) -> &'static str {
        "blob"
    }
    fn serialize(&self) -> String {
        self.data.clone()
    }
    fn deserialize(&mut self, data: &str) {
        self.data = data.to_owned();
    }
    fn as_any(&self) -> &dyn Any {
        self
    }
}

/// Read object `object_id` from `repo`. Return an `Object` whose exact type
/// depends on the object.
pub fn read(repo: &Repository, hash: &str) -> Box<dyn Object> {
    let path = repo::file(repo, &["objects", &hash[..2], &hash[2..]], false).unwrap();

    let file = std::fs::read(path).unwrap();
    let raw = {
        use std::io::Read as _;
        let mut bytes = String::new();
        Decoder::new(&file[..]).unwrap().read_to_string(&mut bytes).unwrap();
        bytes
    };

    // Read object type
    let space_idx = raw.find(' ').unwrap();
    let fmt = &raw[..space_idx];

    // Read and validate object size
    let null_idx = raw[space_idx..].find('\0').unwrap() + space_idx;
    let size = raw[space_idx + 1 .. null_idx].parse::<usize>().unwrap();
    if size != raw.len() - null_idx - 1 {
        panic!("Malformed object {}: bad length", hash);
    }

    let data = &raw[null_idx + 1..];
    match fmt {
        "commit" => Box::new(Commit::from_data(&data)),
        "tree" => Box::new(Tree::from_data(&data)),
        "tag" => Box::new(Tag::from_data(&data)),
        "blob" => Box::new(Blob::from_data(&data)),
        _ => panic!("Unknown type '{}' for object {}", fmt, hash),
    }
}

/// Encode `obj` and calculate its hash.
pub fn encode_and_hash(obj: &dyn Object) -> (String, String) {
    // Serialize object data
    let data = obj.serialize();

    // Add header
    let encoded = format!("{} {}\0{}", obj.fmt(), data.len(), data);

    // Calculate hash
    let hash = sha1::Sha1::from(&encoded).hexdigest();

    (encoded, hash)
}

/// Write `obj` to disk.
pub fn write(obj: &dyn Object, repo: &Repository) -> String {
    // Encode and hash object
    let (encoded, hash) = encode_and_hash(obj);

    // Compute path
    let path = repo::file(repo, &["objects", &hash[..2], &hash[2..]], true).unwrap();
    let file = std::fs::File::create(&path).unwrap();

    // Compress and write
    use std::io::Write as _;
    let mut encoder = Encoder::new(file).unwrap();
    encoder.write_all(encoded.as_bytes()).unwrap();

    hash
}

pub fn find(repo: &Repository, name: &str, typ: &str, follow: bool) -> String {
    if let Some(mut hash) = resolve(repo, name) {
        if typ.is_empty() {
            return hash;
        }
        loop {
            let obj = read(repo, &hash);
            if obj.fmt() == typ {
                return hash;
            }
            if !follow {
                panic!("Incomplete resolution");
            }

            // Follow tags
            hash = if obj.fmt() == "tag" {
                let obj = obj.as_any().downcast_ref::<Tag>().unwrap();
                obj.kvlm.0.iter().find(|(k, _)| k == "object").unwrap().1.clone()
            } else if obj.fmt() == "commit" && typ == "tree" {
                let obj = obj.as_any().downcast_ref::<Commit>().unwrap();
                obj.kvlm.0.iter().find(|(k, _)| k == "tree").unwrap().1.clone()
            } else {
                panic!("Cannot follow object type {}", obj.fmt());
            };
        }
    } else {
        panic!("No such reference: {}", name);
    }
}

/// Resolve a name to an actual object hash in the repo.
///
/// This function is aware of:
///  - the HEAD literal
///  - short and long hashes
///  - tags
///  - branches
///  - remote branches
fn resolve(repo: &Repository, name: &str) -> Option<String> {
    let name = name.trim();

    if name.is_empty() {
        None
    } else if name == "HEAD" {
        Some(super::refer::resolve(repo, "HEAD"))
    } else if name.chars().all(|ch| ch.is_ascii_hexdigit()) {
        if name.len() == 40 {
            // This is a complete hash
            Some(name.to_owned())
        } else if name.len() >= 4 && name.len() < 40 {
            // This is a small hash; 4 seems to be the minimal length
            // for git to consider a short hash. This limit is
            // documented in `man git-rev-parse`.
            let name = name.to_lowercase();
            let (prefix, suffix) = name.split_at(2);
            if let Some(path) = super::repo::dir(&repo, &["objects", prefix], false) {
                let mut candidates = vec![];
                for f in path.read_dir().unwrap() {
                    let file_name = f.unwrap().file_name().to_str().unwrap().to_owned();
                    if file_name.starts_with(suffix) {
                        candidates.push(file_name);
                    }
                }
                if candidates.is_empty() {
                    panic!("No hashes match {}", name);
                } else if candidates.len() > 1 {
                    panic!("Multiple hashes match {}", name);
                } else {
                    Some(candidates[0].clone())
                }
            } else {
                None
            }
        } else {
            // This is too long to be valid
            None
        }
    } else {
        // Possible tag or branch name
        None
    }
}

/// Calculate the hash of a file on disk.
pub fn from_file(file: &mut std::fs::File, typ: &str) -> Box<dyn Object> {
    use std::io::Read as _;
    let mut data = String::new();
    file.read_to_string(&mut data).unwrap();

    // Choose object type
    match typ {
        "commit" => Box::new(Commit::from_data(&data)),
        "tree" => Box::new(Tree::from_data(&data)),
        "tag" => Box::new(Tag::from_data(&data)),
        "blob" => Box::new(Blob::from_data(&data)),
        _ => panic!("Unknown object type '{}'", typ),
    }
}
