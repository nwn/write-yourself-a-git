mod repo;
mod obj;
mod kvlm;
mod tree;
mod refer;

use std::path::PathBuf;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
#[structopt(name = "wyag", about = "The stupid content tracker")]
enum Opt {
    Add {},

    #[structopt(about = "Provide content of repository objects.")]
    CatFile {
        typ: String,
        obj: String,
    },

    #[structopt(about = "Checkout a commit inside of a directory.")]
    Checkout {
        #[structopt(help = "The commit or tree to checkout.")]
        commit: String,
        #[structopt(help = "The EMPTY directory to checkout into.")]
        path: PathBuf,
    },

    Commit {},

    #[structopt(about = "Compute object ID and optionally creates a blob from a file")]
    HashObject {
        #[structopt(short, help = "Actually write the object into the database")]
        write: bool,
        #[structopt(short, name = "type", default_value = "blob", help = "Specify the type of object")]
        typ: String,
        #[structopt(help = "Read object from <file>")]
        file: PathBuf,
    },

    #[structopt(about = "Initialize a new, empty repository.")]
    Init {
        #[structopt(default_value = ".", help = "Where to create the repository.")]
        path: PathBuf,
    },

    #[structopt(about = "Display history of a given commit.")]
    Log {
        #[structopt(default_value = "HEAD", help = "Commit to start at.")]
        commit: String,
    },

    #[structopt(about = "Pretty-print a tree object.")]
    LsTree {
        #[structopt(help = "The object to show.")]
        object: String,
    },

    Merge {},
    Rebase {},

    #[structopt(about = "Parse revision (or other object) identifiers.")]
    RevParse {
        #[structopt(long, default_value, help = "Specify the expected type")]
        wyag_type: String,
        #[structopt(help = "The name to parse")]
        name: String,
    },

    Rm {},

    #[structopt(about = "List references.")]
    ShowRef,

    #[structopt(about = "List and create tags.")]
    Tag {
        #[structopt(short = "a", help = "Whether to create a tag object")]
        create_tag_object: bool,
        #[structopt(help = "The new tag's name")]
        name: Option<String>,
        #[structopt(default_value = "HEAD", help = "The object the new tag will point to")]
        object: String,
    },
}

pub fn main() {
    match Opt::from_args() {
        Opt::CatFile { typ, obj } => {
            let repo = repo::find(".", false).unwrap();
            let obj = obj::read(&repo, &obj::find(&repo, &obj, &typ, true));
            println!("{}", obj.serialize());
        },
        Opt::Checkout { commit, path } => {
            let repo = repo::find(".", false).unwrap();
            let commit = obj::read(&repo, &obj::find(&repo, &commit, "commit", true));
            if let Some(commit) = commit.as_any().downcast_ref::<obj::Commit>() {
                // If the object is a commit, grab its tree.
                let mut tree_hash = "";
                for (key, val) in &commit.kvlm.0 {
                    if key == "tree" {
                        tree_hash = val;
                        break;
                    }
                }
                let tree = obj::read(&repo, tree_hash);
                let tree = tree.as_any().downcast_ref::<obj::Tree>().unwrap();

                std::fs::create_dir_all(&path).unwrap();
                if let Ok(mut iter) = path.read_dir() {
                    if iter.next().is_some() {
                        panic!("Not empty: {}!", path.display());
                    }
                } else {
                    panic!("Not a directory: {}", path.display());
                }

                tree::checkout(&repo, tree, &path.canonicalize().unwrap());
            }
        },
        Opt::HashObject { write, typ, file } => {
            let mut file = std::fs::File::open(&file).unwrap();
            let obj = obj::from_file(&mut file, &typ);
            let hash = if write {
                let repo = repo::Repository::with_path(".", false);
                obj::write(&*obj, &repo)
            } else {
                obj::encode_and_hash(&*obj).1
            };
            println!("{}", hash);
        },
        Opt::Init { path } => {
            repo::create(&path);
        },
        Opt::Log { commit } => {
            let repo = repo::find(".", false).unwrap();
            let obj = obj::find(&repo, &commit, "commit", true).to_owned();

            let mut seen = std::collections::HashSet::new();
            let mut hash = obj.clone();
            'ancestors: while !seen.contains(&hash) {
                seen.insert(hash.clone());
                println!("Commit: {}", hash);

                let commit = obj::read(&repo, &hash);
                let commit = commit.as_any().downcast_ref::<obj::Commit>().unwrap();

                for (key, val) in &commit.kvlm.0 {
                    if key == "parent" {
                        hash = val.clone();
                        continue 'ancestors;
                    }
                }
            }
        },
        Opt::LsTree { object } => {
            let repo = repo::find(".", false).unwrap();
            let tree = obj::read(&repo, &obj::find(&repo, &object, "tree", true));
            let tree = tree.as_any().downcast_ref::<obj::Tree>().unwrap();

            for leaf in &tree.leaves {
                println!("{:06} {} {}\t{}",
                    std::str::from_utf8(&leaf.mode).unwrap(),
                    obj::read(&repo, &leaf.hash).fmt(),
                    leaf.hash,
                    leaf.path,
                );
            }
        },
        Opt::RevParse { wyag_type, name } => {
            let repo = repo::find(".", false).unwrap();
            println!("{}", obj::find(&repo, &name, &wyag_type, true));
        },
        Opt::ShowRef => {
            let repo = repo::find(".", false).unwrap();
            let refs = refer::list(&repo);
            refer::show(&refs);
        },
        Opt::Tag { create_tag_object, name, object } => {
            let repo = repo::find(".", false).unwrap();

            if let Some(name) = name {
                let typ = if create_tag_object { "object" } else { "ref" };
                // tag::create(&name, &object, typ);
                unimplemented!();
            } else {
                let refs = refer::list(&repo);
                refer::show(&refs);
            }
        },
        _ => (),
    }
}
