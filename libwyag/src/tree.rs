use std::path::Path;

pub struct Leaf {
    pub mode: Vec<u8>,
    pub path: String,
    pub hash: String,
}

fn parse_record(mut raw: &str) -> Leaf {
    // Find the space terminator of the mode.
    let sep = raw.find(' ').unwrap();
    // Read the mode.
    let mode = raw[..sep].as_bytes().to_owned();
    raw = &raw[sep+1..];

    // Find the null terminator of the path.
    let sep = raw.find('\0').unwrap();
    // Read the path.
    let path = raw[..sep].to_owned();
    raw = &raw[sep+1..];

    let mut hash = String::with_capacity(40);
    for ch in raw[..20].bytes() {
        hash.push_str(&format!("{:02x}", ch));
    }

    Leaf {
        mode,
        path,
        hash
    }
}

pub fn parse(raw: &str) -> Vec<Leaf> {
    raw.split_terminator('\n')
        .map(|line| parse_record(line))
        .collect()
}

pub fn serialize(tree: &[Leaf]) -> String {
    let mut result = String::new();

    for leaf in tree {
        result.push_str(std::str::from_utf8(&leaf.mode).unwrap());
        result.push(' ');
        result.push_str(&leaf.path);
        result.push('\0');
        for i in (0..leaf.hash.len()).step_by(2) {
            let window = &leaf.hash[i..i+2];
            let value = u8::from_str_radix(window, 16).unwrap();
            result.push(value as char);
        }
        result.push('\n');
    }

    result
}

pub fn checkout(repo: &super::repo::Repository, tree: &super::obj::Tree, path: &Path) {
    for leaf in &tree.leaves {
        let obj = super::obj::read(repo, &leaf.hash);
        let dest = path.join(&leaf.path);

        if let Some(tree) = obj.as_any().downcast_ref::<super::obj::Tree>() {
            std::fs::create_dir(&dest).unwrap();
            checkout(repo, tree, &dest);
        } else if let Some(blob) = obj.as_any().downcast_ref::<super::obj::Blob>() {
            std::fs::write(&dest, &blob.data).unwrap();
        }
    }
}
