use std::path::{Path, PathBuf};
use ini::Ini;

/// A git repository
pub struct Repository {
    work_tree: PathBuf,
    git_dir: PathBuf,
    config: Ini,
}
impl Repository {
    /// Open a repository at the given `path`, failing if `!force` and
    /// the repo does not exist.
    pub fn with_path<P: AsRef<Path>>(path: P, force: bool) -> Self {
        let path = path.as_ref();
        let mut this = Self {
            work_tree: path.to_owned(),
            git_dir: path.join(".git"),
            config: Ini::new(),
        };

        if !force && !this.git_dir.is_dir() {
            panic!("Not a git repository: {}", path.display());
        }

        // Read configuration file in .git/config
        if let Some(config_file) = file(&this, &["config"], false) {
            if config_file.is_file() {
                this.config = Ini::load_from_file(config_file).unwrap();
            } else if !force {
                panic!("Configuration file missing");
            }
        } else if !force {
            panic!("Configuration file missing");
        }

        if !force {
            let version = this.config.get_from(Some("core"), "repositoryformatversion")
                .unwrap()
                .parse::<u32>()
                .unwrap();
            if version != 0 {
                panic!("Unsupported repositoryformatversion {}", version);
            }
        }

        this
    }
}

/// Create a new repository at `path`.
pub fn create(path: &Path) -> Repository {
    let repo = Repository::with_path(path, true);

    // First, we make sure the path either doesn't exist or it is an empty dir.

    if repo.work_tree.exists() {
        if let Ok(mut work_tree) = repo.work_tree.read_dir() {
            if work_tree.next().is_some() {
                panic!("{} is not empty!", path.display());
            }
        } else {
            panic!("{} is not a directory!", path.display());
        }
    } else {
        std::fs::create_dir_all(&repo.work_tree).unwrap();
    }

    assert!(dir(&repo, &["branches"], true).is_some());
    assert!(dir(&repo, &["objects"], true).is_some());
    assert!(dir(&repo, &["refs", "tags"], true).is_some());
    assert!(dir(&repo, &["refs", "heads"], true).is_some());

    // .git/description
    std::fs::write(file(&repo, &["description"], false).unwrap(),
        "Unnamed repository; edit this file 'description' to name the repository.\n").unwrap();

    // .git/HEAD
    std::fs::write(file(&repo, &["HEAD"], false).unwrap(),
        "ref: refs/heads/master\n").unwrap();

    default_config().write_to_file(file(&repo, &["config"], false).unwrap()).unwrap();

    repo
}

/// Find a git repository in this directory or its parents.
pub fn find<P: AsRef<Path>>(path: P, required: bool) -> Option<Repository> {
    let path = path.as_ref().canonicalize().unwrap();

    // Recurse through parents
    for path in path.ancestors() {
        if path.join(".git").is_dir() {
            return Some(Repository::with_path(&path, false));
        }
    }

    if required {
        panic!("No git directory.");
    } else {
        None
    }
}

/// The default initial configuration of a git repository.
fn default_config() -> Ini {
    let mut config = Ini::new();
    config.set_to(Some("core"), "repositoryformatversion".into(), "0".into());
    config.set_to(Some("core"), "filemode".into(), "false".into());
    config.set_to(Some("core"), "bare".into(), "false".into());
    config
}

/// Compute a path under `repo`'s git directory.
fn path(repo: &Repository, path: &[&str]) -> PathBuf {
    repo.git_dir.join(&path.iter().collect::<PathBuf>())
}

/// Same as path, but optionally creates `dirname path` if absent.
///
/// For example, `file(repo, &["refs", "remotes", "origin", "HEAD"])` will
/// create `.git/refs/remotes/origin`.
pub(super) fn file(repo: &Repository, path: &[&str], mkdir: bool) -> Option<PathBuf> {
    if let Some((_, dir_name)) = path.split_last() {
        dir(repo, dir_name, mkdir).map(|_| self::path(repo, path))
    } else {
        None
    }
}

/// Same as path, but optionally creates the path if absent.
pub fn dir(repo: &Repository, path: &[&str], mkdir: bool) -> Option<PathBuf> {
    let path = self::path(repo, path);
    if path.exists() {
        if path.is_dir() {
            Some(path)
        } else {
            panic!("Not a directory: {}", path.display());
        }
    } else if mkdir {
        std::fs::create_dir_all(&path).unwrap();
        Some(path)
    } else {
        None
    }
}
