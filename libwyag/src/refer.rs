use std::path::Path;
use std::collections::BTreeMap;
use super::repo::Repository;

pub fn resolve<P: AsRef<Path>>(repo: &Repository, refer: P) -> String {
    let mut refer = refer.as_ref().to_owned();
    loop {
        let file = super::repo::file(repo, &[refer.to_str().unwrap()], false).unwrap();
        let data = std::fs::read_to_string(&file).unwrap();

        let ref_prefix = "ref: ";
        if data.starts_with(ref_prefix) {
            refer = if data.ends_with('\n') {
                data[ref_prefix.len()..data.len()-1].to_owned()
            } else {
                data[ref_prefix.len()..].to_owned()
            }.into();
        } else {
            break if data.ends_with('\n') {
                data[..data.len()-1].to_owned()
            } else {
                data.to_owned()
            };
        }
    }
}

type Dir = BTreeMap<String, FsNode>;
pub enum FsNode {
    File(String),
    Dir(Dir),
}

pub fn list(repo: &Repository) -> Dir {
    fn recur<P: AsRef<Path>>(repo: &Repository, path: P) -> Dir {
        let path = path.as_ref().to_owned();

        let mut result = BTreeMap::new();
        for entry in path.read_dir().unwrap() {
            if let Ok(entry) = entry {
                let name = entry.file_name().to_str().unwrap().to_owned();
                let canon = entry.path();
                let node = if canon.is_dir() {
                    FsNode::Dir(recur(repo, &canon))
                } else {
                    FsNode::File(resolve(repo, &canon))
                };
                result.insert(name, node);
            }
        }
        result
    }
    let path = super::repo::dir(repo, &["refs"], false).unwrap();
    recur(repo, path)
}

pub fn show(refs: &Dir) {
    fn recur<P: AsRef<Path>>(refs: &Dir, prefix: P) {
        for (key, val) in refs.iter() {
            let path = prefix.as_ref().join(key);
            match val {
                FsNode::File(name) => {
                    println!("{} {}", name, path.display());
                },
                FsNode::Dir(dir) => {
                    recur(dir, &path);
                },
            }
        }
    }
    recur(refs, "");
}
